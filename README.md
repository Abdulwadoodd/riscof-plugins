
**⚠ WARNING: This project has been moved to [new location](https://gitlab.incoresemi.com/core-verification/riscof-plugins.git). It will soon be archived and eventually deleted.**

# riscof-plugins

Contains various model plugins to be used along with RISCOF (https://gitlab.com/incoresemi/riscof)

Each plugin contains a `README.md` file for details on setup, usage and configuration.


## Third-Party Plugins

| Core/DUT | Plugin | Documentation |
|:---------|:-------|:--------------|
| [neorv32](https://github.com/stnolting/neorv32) (GitHub) | [plugin-neorv32](https://github.com/stnolting/neorv32-riscof/tree/main/plugin-neorv32) (GitHub) | [github.com/stnolting/neorv32-riscof](https://github.com/stnolting/neorv32-riscof)
| [serv](https://github.com/olofk/serv) (GitHub) | [plugin-serv](https://github.com/olofk/serv/tree/main/verif/plugin-serv) (GitHub) | https://github.com/olofk/serv/tree/main/verif
| [ibex](https://github.com/Abdulwadoodd/ibex/tree/compliance) (GitHub) |[plugin-ibex](https://github.com/Abdulwadoodd/ibex/tree/compliance/dv/riscv_compliance/plugin-ibex) (GitHub) | [link](https://github.com/Abdulwadoodd/ibex/tree/compliance/dv/riscv_compliance)
| [SweRV-EL2](https://github.com/ALI11-2000/Cores-SweRV-EL2) (GitHub) | [plugin-swerv_el2](https://github.com/ALI11-2000/Cores-SweRV-EL2/tree/master/compliance/swerv_el2) (GitHub) |  [link](https://github.com/ALI11-2000/Cores-SweRV-EL2#running-compliance-tests)
| [SwerRV-EH1](https://github.com/akifejaz/Cores-SweRV) (GitHub) | [plugin-swerv-eh1](https://github.com/akifejaz/Cores-SweRV/tree/master/compliance/swerveh1) (GitHub) | N/A